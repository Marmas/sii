# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/nuria/sii/practica1/src/Esfera.cpp" "/home/nuria/sii/practica1/build/src/CMakeFiles/servidor.dir/Esfera.cpp.o"
  "/home/nuria/sii/practica1/src/MundoServidor.cpp" "/home/nuria/sii/practica1/build/src/CMakeFiles/servidor.dir/MundoServidor.cpp.o"
  "/home/nuria/sii/practica1/src/Plano.cpp" "/home/nuria/sii/practica1/build/src/CMakeFiles/servidor.dir/Plano.cpp.o"
  "/home/nuria/sii/practica1/src/Raqueta.cpp" "/home/nuria/sii/practica1/build/src/CMakeFiles/servidor.dir/Raqueta.cpp.o"
  "/home/nuria/sii/practica1/src/Socket.cpp" "/home/nuria/sii/practica1/build/src/CMakeFiles/servidor.dir/Socket.cpp.o"
  "/home/nuria/sii/practica1/src/Vector2D.cpp" "/home/nuria/sii/practica1/build/src/CMakeFiles/servidor.dir/Vector2D.cpp.o"
  "/home/nuria/sii/practica1/src/servidor.cpp" "/home/nuria/sii/practica1/build/src/CMakeFiles/servidor.dir/servidor.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
