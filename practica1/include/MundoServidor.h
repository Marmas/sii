// Mundo.h: interface for the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	
	int puntos1;
	int puntos2;

	int fd;
	//int fdp3;
	//int fdp3tec;
	Socket SocketConexion;
	Socket SocketCliente;
	char nombre_SocketCliente[20];
	pthread_t th1;

DatosMemCompartida memcomp;
int tmp;
DatosMemCompartida *DatosCompartidos;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
